extends KinematicBody2D


onready var target = position


func _physics_process(_delta):
	target = get_global_mouse_position()
	target.y = position.y
	position = target
