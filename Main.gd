extends Node2D

export (PackedScene) var block_scene : PackedScene
export (PackedScene) var reward_scene : PackedScene


func _ready():
	randomize()
	new_game()


func new_game():
	$StartTimer.start()


func game_over():
	# todo: show ending screen
	
	get_tree().reload_current_scene()


func _on_StartTimer_timeout():
	spawn_new_block()


func _on_block_deleted():
	spawn_new_block()


func spawn_new_block():
	var block = block_scene.instance()

	block.position = $SpawnPosition.position

	block.connect('block_deleted', self, '_on_block_deleted')
	block.connect('player_died', self, '_on_player_died')
	block.connect('reward_achieved', self, '_on_reward_achieved')

	call_deferred("add_child", block)


func _on_player_died():
	game_over()

func _on_reward_achieved(reward_index: int):
	var reward_path
	var reward_trans
	var reward_scale

	match reward_index:
		0:
			reward_path = 'res://Images/blunt.png'
			reward_trans = Vector2(55, 22)
			reward_scale = Vector2(0.25, 0.25)
		1:
			reward_path = 'res://Images/bluehairandpronouns.png'
			reward_trans = Vector2(-55, -40)
			reward_scale = Vector2(0.5, 0.5)
		3:
			reward_path = 'res://Images/koreanwonstack.png'
			reward_trans = Vector2(60, 30)
			reward_scale = Vector2(0.15, 0.15)
		_:
			return

	var reward: Node2D = reward_scene.instance()

	reward.get_node('Sprite').texture = load(reward_path)
	
	reward.transform = reward.transform.translated(reward_trans)
	reward.scale.x = reward_scale.x
	reward.scale.y = reward_scale.y

	$Player.add_child(reward)
