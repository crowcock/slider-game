# Slider
## A bad mobile game

An infinite scrolling game where you dodge bad tiles.

Based on the gif from this post: https://reddit.com/r/196/comments/1161ckk/mobile_ad_rule/

Itch page: https://desecaw13.itch.io/slider
