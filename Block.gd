extends KinematicBody2D


signal block_deleted
signal player_died
signal reward_achieved(reward_index)

export (int) var speed = 500

var velocity = Vector2()
var which_is_bad
var reward_number


func _ready():
	reward_number = randi() % 6
	var good_image = load('res://Images/Blocks/good%d.png' % reward_number)
	var bad_image = load('res://Images/Blocks/bad%d.png' % (randi() % 6))

	if randi() % 2 == 0:
		$LeftSprite.texture = good_image
		$RightSprite.texture = bad_image
		which_is_bad = 'right'
	else:
		$LeftSprite.texture = bad_image
		$RightSprite.texture = good_image
		which_is_bad = 'left'


func _physics_process(_delta):
	velocity = Vector2.DOWN * speed
	velocity = move_and_slide(velocity)


func _on_Area_body_shape_entered(_body_rid, _body, _body_shape_index, local_shape_index):
	var shape: CollisionShape2D = $Area.shape_owner_get_owner(local_shape_index)
	
	var side = ''
	
	if shape == $Area/LeftShape:
		side = 'left'
	if shape == $Area/RightShape:
		side = 'right'
	
	if side == which_is_bad:
		emit_signal("player_died")
	else:
		emit_signal("reward_achieved", reward_number)


func _on_VisibilityNotifier2D_screen_exited():
	emit_signal("block_deleted")
	queue_free()
